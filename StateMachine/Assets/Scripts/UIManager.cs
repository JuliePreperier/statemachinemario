﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Manager for the UI
public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance { get { return instance; } }


    /* TIMER VARIABLES */
    private float time = 0;
    [SerializeField]
    private Text timerText;
    private int timerInt;
    public bool isGameOver = false;

    /* LIFE MANAGER */
    private int life = 3;
    [SerializeField]
    GameObject gameOverPanel;
    [SerializeField]
    private Text lifeText;

    /* COIN MANAGER */
    [SerializeField]
    private Text coinsText;
    private int coins = 0;

    /* POINTS MANAGER */
    [SerializeField]
    private Text pointsText;
    private int points = 0;

    /* GAME OVER MANAGER */
    [SerializeField]
    private Text finalPointsText;
    [SerializeField]
    private Text finalTimeText;
    [SerializeField]
    private Button restartButton;

    private void Awake()
    {
        // Save several variable if we have to reload the scene without loosing points,coins or life
        instance = this;
        if (PlayerPrefs.HasKey("CurrentLife") || PlayerPrefs.HasKey("CurrentPoints") || PlayerPrefs.HasKey("CurrentCoins"))
        {
            life = PlayerPrefs.GetInt("CurrentLife");
            lifeText.text = (life + "");
            points = PlayerPrefs.GetInt("CurrentPoints");
            pointsText.text = (points + "");
            coins = PlayerPrefs.GetInt("CurrentCoins");
            coinsText.text = (coins + "");
        }
        else
        {
            Save();
        }
    }

    // Save the variable we want to keep
    public void Save()
    {
        PlayerPrefs.SetInt("CurrentLife", life);
        PlayerPrefs.SetInt("CurrentPoints", points);
        PlayerPrefs.SetInt("CurrentCoins", coins);
    }

    void Start()
    {
        gameOverPanel.SetActive(false);
        lifeText.text = (life + "");
    }

    void Update()
    {
        /* TIMER WORKING */
        if (!isGameOver)
        {
            timerInt = Mathf.RoundToInt(time);
            timerText.text = (timerInt + "");
            time += Time.deltaTime;
        }
    }
    
    public void LooseLife()
    {
        life--;
        lifeText.text = (life + "");

        if (life <= 0)
        {
            isGameOver = true;
            life = 0;
            lifeText.text = (life + "");
            PlayerPrefs.DeleteAll();
            gameOverPanel.SetActive(true);
            Time.timeScale = 0;
            finalPointsText.text = (points + "");
            finalTimeText.text = (time + "");
        }
        Save();
    }

    public void AddLife()
    {
        life++;
        lifeText.text = (life + "");
        finalTimeText.text = (time + "");
        finalPointsText.text = (points + "");
        Save();
    }

    public void AddCoins()
    {
        coins++;
        coinsText.text = (coins + "");
        if(coins == 100)
        {
            SoundManager.instance.playSound("1up");
            AddLife();
            coins = 0;
            coinsText.text = (coins + "");
        }
        Save();
    }

    public void AddPoints(int nbpoints)
    {
        points += nbpoints;
        pointsText.text = (points + "");
        Save();
    }

    // Method for button
    public void RestartGames()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
    }

}
