﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    AudioSource musicAudioSource;
    AudioSource soundAudioSource;
    [Serializable]
    struct SoundInfos
    {
        public string key;
        public AudioClip clip;
        public bool isMusic;
    }
    [SerializeField]
    SoundInfos[] sounds;

    void Start()
    {
        instance = this;
        AudioSource[] audioSources = GetComponents<AudioSource>();
        musicAudioSource = audioSources[0];

        soundAudioSource = audioSources[1];
        playSound("theme");
    }

    public void playSound(string key)
    {
        for(int i=0; i < sounds.Length; i++)
        {
            if(sounds[i].key == key)
            {
                if (sounds[i].isMusic)
                {
                    musicAudioSource.clip = sounds[i].clip;
                    musicAudioSource.Play();
                }
                else
                {
                    soundAudioSource.clip = sounds[i].clip;
                    soundAudioSource.Play();
                }
                
                break;
            }
        }
    }

    void Update()
    {
        
    }
}
