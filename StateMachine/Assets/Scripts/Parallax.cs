﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    Camera camera;
    [SerializeField]
    float factor;
    float initX;
    float initXCamera;
    void Start()
    {
        camera = Camera.main;
        initX = transform.position.x;
        initXCamera = camera.transform.position.x;
    }

    
    void Update()
    {
        // Si le factor est positif le plan va se déplacer dans le meme sens que la camera et s'il est négatif il va se déplacer à l'inverse de la camera
        transform.position = new Vector3(initX+(factor*(camera.transform.position.x-initXCamera)), transform.position.y, transform.position.z);
    }
}
