﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyBehaviour : MonoBehaviour
{
    bool isSmash = false;
    Animator animator;
    LeftRightBehaviour scriptRightLeft;
    Rigidbody2D rb;
    private GameObject UIPannel;
    [SerializeField]
    private int givenPoints;



    private void Start()
    {
        // ignore collider of camera
        Physics2D.IgnoreCollision(Camera.main.GetComponent<Collider2D>().GetComponent<Collider2D>(), GetComponent<Collider2D>());
        UIPannel = GameObject.Find("UI");
        animator = GetComponent<Animator>();
        scriptRightLeft = GetComponent<LeftRightBehaviour>();
        rb = GetComponent<Rigidbody2D>();
    }


    public void SetStatic(bool isStatic)
    {
        if (isStatic)
        {
            rb.bodyType = RigidbodyType2D.Static;
        }
        else
        {
            rb.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            // if hit by player from above -> get smash
            if (!isSmash && collision.GetContact(collision.contactCount / 2).normal.y < -0.5f)
            {
                SoundManager.instance.playSound("stomp");
                isSmash = true;
                animator.SetBool("isSmash", true);
                UIPannel.GetComponent<UIManager>().AddPoints(givenPoints);
                UIManager.Instance.Save();
                GetComponent<Collider2D>().enabled = false;
                rb.gravityScale = 0;
                rb.velocity = Vector2.zero;
                scriptRightLeft.enabled = false;
                Destroy(gameObject, 1);
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
            }
            else
            {
                collision.gameObject.GetComponent<MarioController>().HitByEnnemy(this.gameObject);
            }
            //if hit by fireball -> get destroy
        }if(collision.gameObject.tag == "FireBall")
        {
            SoundManager.instance.playSound("kick");
            UIPannel.GetComponent<UIManager>().AddPoints(givenPoints);
            GetComponent<Collider2D>().enabled = false;
            rb.gravityScale = 0;
            rb.velocity = Vector2.zero;
            scriptRightLeft.enabled = false;
            Destroy(gameObject, 1);
            Destroy(collision.gameObject);
        }
    }
}
