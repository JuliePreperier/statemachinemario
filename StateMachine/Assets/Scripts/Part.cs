﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour
{
    [SerializeField]
    GameObject end;
    [SerializeField]
    GameObject[] nextPossibleParts;
    bool isAlreadyGenerated;

    
    void Update()
    {
        if(!isAlreadyGenerated && Camera.main.transform.position.x > transform.position.x)
        {
            GameObject newPart = Instantiate(nextPossibleParts[Random.Range(0, nextPossibleParts.Length)]);
            newPart.transform.position = end.transform.position;
            isAlreadyGenerated = true;
        }
        if(Camera.main.transform.position.x  > end.transform.position.x + Camera.main.aspect * Camera.main.orthographicSize)
        {
            Destroy(gameObject);
        }
    }
}
