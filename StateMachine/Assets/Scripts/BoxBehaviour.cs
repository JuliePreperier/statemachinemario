﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class BoxBehaviour : MonoBehaviour
{
    bool isOpen = false;
    Animator animator;
    [SerializeField]
    List<GameObject> powerUps;
    bool isCoins = false;
    [SerializeField]
    GameObject coins;

    private void Start()
    {
        animator = GetComponent<Animator>();
        // 70% de chance que ca ne soit qu'une pièce
        if(Random.Range(0, 10) < 7)
        {
            isCoins = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && !isOpen && collision.GetContact(collision.contactCount/2).normal.y >0.5f)
        {
            // Instantiate coin
            if (isCoins)
            {
                isOpen = true;
                animator.SetBool("isOpen", true);
                GameObject newObj = Instantiate(coins, transform);
                newObj.transform.localPosition = new Vector3(0, 0.1f, 0);
            }
            // Instantiate power Up
            else
            {
                isOpen = true;
                animator.SetBool("isOpen", true);
                GameObject newObj = Instantiate(powerUps[Random.Range(0, powerUps.Count - 1)], transform);
                newObj.transform.localPosition = new Vector3(0, 0.1f, 0);
            }
        }
    }
}
