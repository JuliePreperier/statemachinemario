﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopPowerUp : MonoBehaviour
{
    Vector3 target = new Vector3(0, 0.6f,0);
    MonoBehaviour[] monob;
    Collider2D collider;
    Rigidbody2D rigidBody;
    void Start()
    {
        monob = GetComponents<MonoBehaviour>();
        collider = GetComponent<Collider2D>();
        if (GetComponent<Rigidbody2D>())
        {
            rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.gravityScale = 0;
        }

        if (!collider.isTrigger)
        {
            collider.enabled = false;
        }

        foreach (MonoBehaviour m in monob)
        {
            if(m.GetType() != this.GetType())
            {
                m.enabled = false;
            }
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Vector3.Distance(target,transform.localPosition) < 0.05){
            foreach (MonoBehaviour m in monob)
            {
                if (m.GetType() != this.GetType())
                {
                    m.enabled = true;
                }
            }
            collider.enabled = true;
            if (GetComponent<Rigidbody2D>())
            {
                rigidBody.gravityScale = 1;
            }
            enabled = false;
        }
        transform.localPosition = Vector3.Lerp(transform.localPosition, target, 0.1f);
    }
}
