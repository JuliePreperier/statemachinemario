﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    public Rigidbody2D rb;
    public Vector2 velocity;


    void Start()
    {
        Destroy(this.gameObject,10);
        rb = GetComponent<Rigidbody2D>();
        velocity = rb.velocity;
    }

    
    void Update()
    {
        Debug.Log("Velocity x; "+velocity.x+ ", Velocity y; " + velocity.y);

        if (rb.velocity.y < velocity.y)
        {
            rb.velocity = velocity;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        rb.velocity = new Vector2(velocity.x, -velocity.y);

        // get destroy if hit side of collider
        if (collision.contacts[0].normal.x != 0 && collision.gameObject.tag !="Player")
        {
            Destroy(this.gameObject);
        }
    }
}
