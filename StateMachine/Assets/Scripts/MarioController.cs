﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MarioController : MonoBehaviour
{
    public enum State { Dead, Small, Big, Fire, Leaf, Frog };
    [SerializeField]
    private State currentState = State.Small;
    private CapsuleCollider2D collider;


    private GameObject collidedEnemy;

    [SerializeField]
    private float speedFactor;
    [SerializeField]
    private float jumpForce;

    private Rigidbody2D rigidBody;
    private Animator animator;
    private SpriteRenderer sr;

    private GameObject UIPannel;

    [SerializeField]
    private GameObject fireBall;
    [SerializeField]
    private float throwForce;

    private bool isThrowingBall=false;


     bool doTransition;
     float startTimeTransition = 0;

    void Start()
    {
        UIPannel = GameObject.Find("UI");
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        collider = GetComponent<CapsuleCollider2D>();
    }


    void Update()
    {
        if(currentState == State.Dead)
        {
            if(transform.position.y < -10)
            {
                UIPannel.GetComponent<UIManager>().LooseLife();
                if (!UIPannel.GetComponent<UIManager>().isGameOver)
                {
                    UIManager.Instance.Save();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }
            return;
        }

        if (doTransition)
        {
            Time.timeScale = 0;

            sr.enabled = Time.realtimeSinceStartup % 0.09 > 0.045f;


            if (startTimeTransition+0.25 < Time.realtimeSinceStartup)
            {
                sr.enabled = true;
                Time.timeScale = 1;
                doTransition = false;
            }
        }

        bool isGrounded = Physics2D.Raycast(transform.position, Vector2.down, 0.03f,256);

        float axisX = Input.GetAxis("Horizontal");
        float axisY = Input.GetAxisRaw("Vertical");
        float speedX = axisX * speedFactor;

        if (axisY < -0.5f && isGrounded)
        {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
            animator.SetBool("isCrouched", true);
            if(currentState != State.Small)
            {
                collider.size = new Vector2(0.4f, 0.56f);
                collider.offset = new Vector2(0.01f, 0.28f);
            }
        }
        else
        {
            animator.SetBool("isCrouched", false);
            if(currentState == State.Small)
            {
                collider.size = new Vector2(0.375f, 0.45f);
                collider.offset = new Vector2(0, 0.24f);
            }
            else
            {
                collider.size = new Vector2(0.4f, 0.8f);
                collider.offset = new Vector2(-0.02f, 0.4f);
            }
            rigidBody.velocity = new Vector2(speedX, rigidBody.velocity.y);

            if (Mathf.Abs(rigidBody.velocity.x) > 0.05)
            {
                sr.flipX = rigidBody.velocity.x < 0;
            }
        }

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            if(currentState == State.Small)
            {
                SoundManager.instance.playSound("jumpSmall");
            }
            else
            {
                SoundManager.instance.playSound("jumpBig");
            }
            
            rigidBody.AddForce(new Vector2(0, jumpForce));
        }
        // PRESS 'C' to throw fireball
        if(currentState == State.Fire && Input.GetButtonDown("Fire"))
        {
            SoundManager.instance.playSound("fireball");
            isThrowingBall = true;
            animator.SetBool("isThrowingBall", isThrowingBall); 
            GameObject fire = Instantiate(fireBall, new Vector3(transform.position.x+0.4f, transform.position.y+0.1f, transform.position.z), Quaternion.identity,transform);
            fire.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x, -1)*throwForce;
        }
        
        animator.SetFloat("speed", Mathf.Abs(rigidBody.velocity.x));
        animator.SetBool("isJumping", !isGrounded);
        
    }

    void UpdateStateBehaviour()
    {
        switch (currentState)
        {
            case State.Dead:
                break;
            case State.Small:
                break;
            case State.Big:
                break;
            case State.Fire:
                break;
            case State.Frog:
                break;
            case State.Leaf:
                break;
        }
    }

    public void HitByEnnemy(GameObject enemy)
    {
        collidedEnemy = enemy;
        StartCoroutine(InvincibleAfterHit());

        if (currentState == State.Small)
        {
            SoundManager.instance.playSound("death");
            ChangeState(State.Dead);  
        }
        else if(currentState == State.Big)
        {
            ChangeState(State.Small);
            SoundManager.instance.playSound("powerDown");
        }
        else if(currentState != State.Dead)
        {
            ChangeState(State.Big, true);
            SoundManager.instance.playSound("powerDown");
        }
    }

    // event for animation
    public void ThrowEvent()
    {
        animator.SetBool("isThrowingBall", false);
    }

    // get invincible after get hit
    IEnumerator InvincibleAfterHit()
    {
        doTransition = true;
        startTimeTransition = Time.realtimeSinceStartup;
        Physics2D.IgnoreCollision(collidedEnemy.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        yield return new WaitForSeconds(1.0f);
        Physics2D.IgnoreCollision(collidedEnemy.GetComponent<Collider2D>(), GetComponent<Collider2D>(),false);
    }


    void ChangeState(State newState, bool force=false)
    {
        if(currentState == newState)
        {
            return;
        }

        if(newState == State.Big && currentState != State.Small && !force)
        {
            return;
        }
        currentState = newState;
        switch (newState)
        {
            case State.Small:
                animator.SetTrigger("small");
                doTransition = true;
                startTimeTransition = Time.realtimeSinceStartup;
                collider.size = new Vector2(0.375f, 0.45f);
                collider.offset = new Vector2(0, 0.2f);
                break;
            case State.Big:
                animator.SetTrigger("big");
                doTransition = true;
                startTimeTransition = Time.realtimeSinceStartup;
                collider.size = new Vector2(0.4f, 0.8f);
                collider.offset = new Vector2(-0.02f, 0.4f);
                break;
            case State.Fire:
                animator.SetTrigger("fire");
                doTransition = true;
                startTimeTransition = Time.realtimeSinceStartup;
                collider.size = new Vector2(0.4f, 0.8f);
                collider.offset = new Vector2(-0.02f, 0.4f);
                break;
            case State.Frog:
                animator.SetTrigger("frog");
                doTransition = true;
                startTimeTransition = Time.realtimeSinceStartup;
                collider.size = new Vector2(0.4f, 0.8f);
                collider.offset = new Vector2(-0.02f, 0.4f);
                break;
            case State.Leaf:
                animator.SetTrigger("leaf");
                doTransition = true;
                startTimeTransition = Time.realtimeSinceStartup;
                collider.size = new Vector2(0.4f, 0.8f);
                collider.offset = new Vector2(-0.02f, 0.4f);
                break;
            case State.Dead:
                animator.SetBool("isDead", true);
                collider.enabled = false;
                rigidBody.velocity = Vector2.zero;
                rigidBody.gravityScale = 0;
                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
                foreach (GameObject enemy in enemies)
                {
                    enemy.GetComponent<EnnemyBehaviour>().SetStatic(true);
                    enemy.GetComponent<Animator>().enabled = false;
                }
                
                StartCoroutine(JumpAfterTime(1.5f, () => {
                    rigidBody.gravityScale = 1;
                    rigidBody.AddForce(new Vector2(0, jumpForce/2));
                    return true;
                }));
                
                break;
        }
    }


    private IEnumerator JumpAfterTime(float time, Func<bool> toExec)
    {
        yield return new WaitForSeconds(time);
        toExec.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Coin")
        {
            SoundManager.instance.playSound("coin");
            // Add +1 aux compteur de pièce
            UIPannel.GetComponent<UIManager>().AddCoins();
            UIManager.Instance.Save();
            Destroy(collision.gameObject);
        }
        else if (collision.GetComponent<PowerUp>() !=null)
        {
            SoundManager.instance.playSound("powerUp");
            // If already in shap of the poser up, give 100 pts
            if (collision.GetComponent<PowerUp>().behaviour == currentState)
            {
                UIPannel.GetComponent<UIManager>().AddPoints(100);
            }
            else
            {
                ChangeState(collision.GetComponent<PowerUp>().behaviour);
            }
            if(collision.transform.parent!=null && collision.transform.parent.GetComponent<LeftRightBehaviour>() != null)
            {
                Destroy(collision.transform.parent.gameObject);
            }
            else
            {
                Destroy(collision.gameObject);
            }
        }else if (collision.gameObject.tag == "DeathTrigger")
        {
            SoundManager.instance.playSound("death");
            ChangeState(State.Dead);
        }
    }
}
