﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class LeftRightBehaviour : MonoBehaviour
{
    [SerializeField]
    float speed;
    Rigidbody2D rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();    
    }

    
    void Update()
    {
        rigidBody.velocity = new Vector2(speed, rigidBody.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ContactPoint2D contact = collision.GetContact(collision.contactCount / 2);
        if(Mathf.Abs(contact.normal.x) > Mathf.Abs(contact.normal.y))
        {
            speed *= -1;
        }
    }

}
