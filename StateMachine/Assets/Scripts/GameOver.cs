﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    private void OnEnable()
    {
        Time.timeScale = 1;
        SoundManager.instance.playSound("gameOver");
        Time.timeScale = 0;
    }
}
